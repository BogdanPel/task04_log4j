package com.epam;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSms {

    public static final String ACCOUNT_SID = "AC756157c4d8e3226e402af1822a8a05f3";
    public static final String AUTH_TOKEN = "ab7d3d9802a27a34f0333d359e40c386";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380633971154"), /*my phone number*/
                        new PhoneNumber("+17242137033"), str).create(); /*attached to me number*/
    }

  public static void main(String[] args) {
    send("GoodNignt");
  }
}
